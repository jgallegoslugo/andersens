// EECS 221 - Program Analysis
// Assignment 2: Anderson's Pointer Analysis
// Authors: Julian Lettner and Jose Gallegos
//
// Writes out a graphviz (DOT) file for a points-to mapping.
//

#ifndef DotFileWriter_H
#define DotFileWriter_H

#include <map>
#include <set>
#include "PointsTo.h"

void writeGraph(std::string moduleName, std::map<Var, std::set<Var> >& pointsTo);

#endif
