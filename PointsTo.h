// EECS 221 - Program Analysis
// Assignment 2: Anderson's Pointer Analysis
// Authors: Julian Lettner and Jose Gallegos
//
// Class PointsTo implements Anderson's pointer analysis
// and is used by the AndersonAnalysis pass.
// Helper structs: Var, Assignment.
// 

#ifndef PointsTo_H
#define PointsTo_H

#include <map>
#include <set>
#include <string>
#include <list>
#include <vector>
#include "llvm/Support/raw_ostream.h"

// Struct that stores information about a 
// variable defined in the program.
struct Var {
	enum Type {Constant, PrimitiveVar, Pointer} type;

	std::string name;

	Var() : name("") { }

	// Needed so we can use Var as the key in std::map (sorted map).
	bool operator <(const Var& other) const {
		return name < other.name;
	}

	// So we can conveniently compare Vars by name.
	bool operator ==(const Var& other) const {
		return name == other.name;
	}
	bool operator !=(const Var& other) const {
		return !(*this == other);
	}
};
llvm::raw_ostream& operator <<(llvm::raw_ostream& os, const Var& v);

// Struct that stores information about an assignment.
// enum Type indicates which constraint rule will be
// used to update the points-to graph.
// This struct also stores the variables associated with
// the assignment (lhs, rhs).
struct Assignment {
	enum Type {Alloc, Copy, Load, Store, Call, Return} type;

	Var lhs, rhs;

	Assignment() { }

	Assignment(Type t, Var lhs, Var rhs):
		type(t), lhs(lhs), rhs(rhs) { }
};
llvm::raw_ostream& operator <<(llvm::raw_ostream& os, const Assignment& a);

class PointsTo {
	std::map<Var, std::set<Var> > pointsTo;
	// Used to keep a list of the complex assignments (load, store)
	// to be used in the worklist.
	std::list<Assignment> loads, stores;
	std::vector<Assignment> assignments;
	std::list<Assignment> complex;

public:
	// This must not be a reference, i.e., we ant a copy of the Assignment 'a'.
	void update(Assignment a);
	std::map<Var, std::set<Var> >& finish();
	void print();

private:
	void alloc(Var lhs, Var rhs);
	void copy(Var lhs, Var rhs);
	void reverseCopy(Var lhs, Var rhs);
	void load(Var lhs, Var rhs);
	void store(Var lhs, Var rhs);
	void call(Var lhs, Var rhs);
	void ret(Var lhs, Var rhs);
	bool isPointer(Var var);
	bool isComplexAssignment(const Assignment &a) const;
	bool hasEdge(const Var &src, const Var &dest);
	std::set<Var> getPointers(Var key);
	void load(Assignment load, Assignment store);
	void store(Assignment load, Assignment store);
	bool isTemporary(std::string name);
	// For debugging only.
	void printEdge(const Var &src, const Var &dest);
};

#endif
