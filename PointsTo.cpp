// EECS 221 - Program Analysis
// Assignment 2: Anderson's Pointer Analysis
// Authors: Julian Lettner and Jose Gallegos
//

// Interface
#include "PointsTo.h"

#include <iostream>

using namespace std;
using namespace llvm;

// Output operators for Assignment and Var.
llvm::raw_ostream& operator <<(llvm::raw_ostream& os, const Assignment& a) {
    os << a.lhs << " = " << a.rhs << " (type: " << a.type << ")";
    return os;
}
llvm::raw_ostream& operator <<(llvm::raw_ostream& os, const Var& v) {
  os << v.name;
  return os;
}
  
void PointsTo::update(Assignment a) {
	switch(a.type)
	{
		case Assignment::Alloc : alloc(a.lhs, a.rhs); break;
		case Assignment::Copy : copy(a.lhs, a.rhs); break;
		case Assignment::Load : load(a.rhs, a.lhs); loads.push_front(a); break;
		case Assignment::Store : store(a.lhs, a.rhs); stores.push_front(a); break;
		case Assignment::Call : call(a.lhs, a.rhs); break;
		case Assignment::Return : ret(a.lhs, a.rhs); break;
	}

	assignments.insert(assignments.end(), a);

}

map<Var, std::set<Var> >& PointsTo::finish() {
  
  	int space = 0;
  	map<Var, set<Var> > worklist;

	while(worklist != pointsTo)
	{
		worklist.clear();

		//worklist = pointsTo
		worklist.insert(pointsTo.begin(), pointsTo.end());

		for(vector<Assignment>::iterator it = assignments.begin(); it != assignments.end(); it++)
		{
			space = 0;
			Assignment a = *it;

			if(a.type == Assignment::Load && !isTemporary(it->rhs.name))
			{
				space++;
				it++;
				
				while(it != assignments.end())
				{
					if(isTemporary(it->lhs.name) && isTemporary(it->rhs.name))
					{
						it++;
						space++;
					}
					else
						break;
				}

				//we have edges to reassign
				if(it->type == Assignment::Store)
				{
					//this is the case of a copy assigment
					if(space == 1 && !isTemporary(it->lhs.name))
					{
						copy(it->lhs, a.rhs);
					}
					else if(space == 1 && isTemporary(it->lhs.name))
					{
						reverseCopy(it->lhs, a.rhs);
					}
					else if(space == 2)
					{	
						store(a.rhs, it->lhs);
					}
				}
				else if(it->type == Assignment::Load)
				{
					//this is the case of a store assignment
					if(space == 1)
					{
						store(*it, a);
					}

				}
			}
		}
	}

	return pointsTo;
}

void PointsTo::alloc(Var lhs, Var rhs) {
	//to prevent cycles
	if(lhs == rhs)
		return;
	
	//for instructions using malloc(), there is no
	//right hand side parameter. All we do is create a key = lhs
	//and value = empty set
	if(rhs.name.substr(0, 10) == "alloc site")
	{
		set<Var> empty;
		pointsTo.insert(pair<Var,set<Var> >(lhs, empty));
	}
}

void PointsTo::copy(Var lhs, Var rhs) {
	//to prevent cycles
	if(lhs == rhs)
		return;

	set<Var> pointers = getPointers(rhs);
	
	for(set<Var>::iterator it = pointers.begin(); it != pointers.end(); it++)
	{
		if(lhs != *it)
			pointsTo[lhs].insert(*it);
	}

}

void PointsTo::reverseCopy(Var lhs, Var rhs)
{
	//to prevent cycles
	if(lhs == rhs)
		return;

	set<Var> pointers = getPointers(rhs);
	
	for(set<Var>::iterator it = pointers.begin(); it != pointers.end(); it++)
	{
		if(lhs != *it)
			pointsTo[*it].insert(lhs);
	}
}

void PointsTo::load(Var lhs, Var rhs) {
	//to prevent cycles
	if(lhs == rhs)
		return;

	if(isPointer(lhs) && isPointer(rhs))
		pointsTo[lhs].insert(rhs);
}

void PointsTo::store(Var lhs, Var rhs) {
	//to prevent cycles
	if(lhs == rhs)
		return;
	
	if(isPointer(lhs) && isPointer(rhs))
		pointsTo[lhs].insert(rhs);
}

bool PointsTo::isPointer(Var var) {
	return var.type == Var::Pointer;
}

std::set<Var> PointsTo::getPointers(Var key) {
	//if key isn't in the map, return empty set
	if(pointsTo.find(key) == pointsTo.end())
		return set<Var>();

	return pointsTo.find(key)->second;
}

void PointsTo::print() {
	map<Var, set<Var> >::iterator it;

	for(it = pointsTo.begin(); it != pointsTo.end(); it++)
	{
		set<Var> values = it->second;
		set<Var>::iterator v = values.begin();

		errs() << it->first.name << " -> {" << v->name;
		v++;
		for(; v != values.end(); v++)
		{
			errs() << ", " << v->name;
		}
		errs() << "}\n";
	}
}

void PointsTo::store(Assignment load, Assignment store) {

	Var lhs = load.lhs;

	set<Var> pointers = getPointers(lhs);
	
	for(set<Var>::iterator it = pointers.begin(); it != pointers.end(); it++)
	{
		copy(*it, store.rhs);
	}
}

bool PointsTo::isTemporary(string name)
{
	return name.substr(0, 5) == "temp ";
}
