// EECS 221 - Program Analysis
// Assignment 2: Anderson's Pointer Analysis
// Authors: Julian Lettner and Jose Gallegos
//
// A pass that implements Anderson's pointer analysis and
// outputs analysis results as a points-to graph (dot file).
// Uses PointsTo for the actual algorithm implementation.
//

#define DEBUG_TYPE "AndersonAnalysis"

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Pass.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Assembly/Writer.h"
#include "PointsTo.h"
#include "DotFileWriter.h"

using namespace std;
using namespace llvm;

namespace {
  // Initialize a set with memory function names for C.
  static string tmp[] = { "malloc", "realloc", "calloc", "alloca" };
  static set<string> memoryFuncs(tmp, tmp + sizeof(tmp) / sizeof(tmp[0]));

  struct AndersonAnalysis : public ModulePass {
    static char ID;
    AndersonAnalysis() : ModulePass(ID) {}

    PointsTo pointsTo;

    void update(Assignment& a) {
      errs() << "  " << a << "\n";
      pointsTo.update(a);
    }

    virtual bool runOnModule(Module& module) {
      string moduleName = module.getModuleIdentifier();
      errs() << "Running AndersonAnalysis pass on module '" << moduleName << "'.\n";

      for (Module::iterator f = module.begin(), e = module.end(); f != e; ++f) {
        errs() << "Analyzing instructions for function '" << f->getName() << "'.\n";
        SlotTracker st(&(*f));
        unsigned allocSites = 1;

        for (inst_iterator inst = inst_begin(f); inst != inst_end(f); ++inst) {
          Assignment a;
          if (translate(&(*inst), a, st, &allocSites))
            update(a);
        }
      }

      map<Var, set<Var> >& graph = pointsTo.finish();
      writeGraph(moduleName, graph);

      // We did not modify the module.
      return false;
    }

    virtual void getAnalysisUsage(AnalysisUsage& au) const {
      // The CFG is implicitly maintained by LLVM. No addRequired<T>() needed.

      // We don't modify the program, so we preserve all analyses.
      au.setPreservesAll();
    }

    bool translate(Instruction* inst, Assignment& a, SlotTracker& st, unsigned* allocSites) {
      // errs() << "llvm instruction: " << inst << "\n";

      if (AllocaInst* alloca = dyn_cast<AllocaInst>(inst)) {
        translate(alloca, a.lhs, st);
        a.rhs.name = "variable/pointer definition";
        a.type = Assignment::Alloc;

        return true;
      }

      if (StoreInst* store = dyn_cast<StoreInst>(inst)) {
        Value* lhs = store->getPointerOperand();
        Value* rhs = store->getValueOperand();

        translate(lhs, a.lhs, st);
        translate(rhs, a.rhs, st);
        a.type = Assignment::Store;

        return true;
      }

      if (LoadInst* load = dyn_cast<LoadInst>(inst)) {
        // Left hand side is the load itself.
        Value* rhs = load->getPointerOperand();

        translate(load, a.lhs, st);
        translate(rhs, a.rhs, st);
        a.type = Assignment::Load;

        return true;
      }

      if (CallInst* call = dyn_cast<CallInst>(inst)) {
        Function* f = call->getCalledFunction();
        if (f != NULL) {
          string fname = f->getName();
          // Special case for memory management functions.
          if (memoryFuncs.find(fname) != memoryFuncs.end()) {
            unsigned allocSite = (*allocSites)++;
            //string s = to_string(42); C++ 11 (not available in LLVM :/)
            string str;
            raw_string_ostream os(str);
            os << "alloc site #" << allocSite << " (" << f->getName() << ")";
            a.rhs.name = os.str();
            a.rhs.type = Var::Pointer;
          } else {
            // Synthesize assignments for argument to parameter bindings: param1 = arg1, ...
            unsigned i = 0;
            for(Function::arg_iterator a = f->arg_begin(); a != f->arg_end(); a++, i++) {
              Argument* lhs = a;
              Value* rhs = call->getArgOperand(i);

              Assignment binding;
              translate(lhs, binding.lhs, st);
              translate(rhs, binding.rhs, st);
              binding.type = Assignment::Store;

              update(binding);
            }
          }
          a.rhs.name = fname + "_return";
        }
        
        translate(call, a.lhs, st);
        a.type = Assignment::Store;

        return true;
      }

      return false;
    }

    void translate(Value* val, Var& v, SlotTracker& st) {
      v.name = extractName(val, st);
      v.type =
          isa<Constant>(val) ? Var::Constant :
          val->getType()->isPointerTy() ? Var::Pointer : Var::PrimitiveVar;
    }

    string extractName(Value *value, SlotTracker& tempStore) {
      string str;
      raw_string_ostream os(str);

      if (!value->getType()->isVoidTy()) {
        // if (value->getType()->isPointerTy())
        //   os << "*";

        const Constant *constant = dyn_cast<Constant>(value);
        if (value->hasName()) {
          os << value->getName();
        } else if (constant && !isa<GlobalValue>(constant)) {
          os << *constant;
        } else if (const GlobalValue *global = dyn_cast<GlobalValue>(value)) {
          os << "global #" << tempStore.getGlobalSlot(global);
        } else {
          os << "temp #" << tempStore.getLocalSlot(value);
        }
      } else {
        os << "VOID"; // Should never happen with our algorithm.
      }

      Function* f = NULL;
      if (Instruction* inst = dyn_cast<Instruction>(value))
        f = inst->getParent()->getParent();
      else if (Argument* arg = dyn_cast<Argument>(value))
        f = arg->getParent();

      if (f != NULL)
        os << " (" << f->getName() << ")";

      // os << "<";
      // value->getType()->print(os);
      // os << ">";
      return os.str();
    }
  };
}

char AndersonAnalysis::ID = 0;
static RegisterPass<AndersonAnalysis>
X("AndersonAnalysis", "Outputs a graph that shows the result of Anderson's pointer analysis.");
